# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from proteus import Model
import datetime
from trytond.modules.account.tests.tools import (create_chart, get_accounts)

today = datetime.date.today()


def _create_template(company):
    ProductUom = Model.get('product.uom')
    ProductTemplate = Model.get('product.template')
    ProductCategory = Model.get('product.category')

    unit, = ProductUom.find([('name', '=', 'Unit')])
    create_chart(company)
    accounts = get_accounts(company)

    account_category = ProductCategory(name="Account Category")
    account_category.accounting = True
    revenue = accounts['revenue']
    expense = accounts['expense']
    account_category.account_expense = expense
    account_category.account_revenue = revenue
    account_category.save()

    template = ProductTemplate()
    template.name = 'product'
    template.default_uom = unit
    template.salable = True
    template.list_price = Decimal('30')

    template.account_category = account_category
    template.save()
    return template


def _create_sale(product, customer):
    Sale = Model.get('sale.sale')
    SaleLine = Model.get('sale.line')

    sale = Sale()
    sale.sale_date = today
    sale.party = customer
    sale.invoice_method = 'order'
    sale_line = SaleLine()
    sale.lines.append(sale_line)
    sale_line.product = product
    sale_line.quantity = 2.0
    sale_line = SaleLine()
    sale.lines.append(sale_line)
    sale_line.type = 'comment'
    sale_line.description = 'Comment'
    sale_line = SaleLine()
    sale.lines.append(sale_line)
    sale_line.product = product
    sale_line.quantity = 3.0
    sale.save()
    sale.click('quote')
    sale.click('confirm')
    return sale


def create_sale_forecast(company, customer, quantity=10, product=None,
        create_sale=True):
    SaleForecast = Model.get('sale.forecast')
    if not product:
        template = _create_template(company)
        product, = template.products
    if create_sale:
        _create_sale(product, customer)
    sale_forecast = SaleForecast()
    sale_forecast.customer = customer
    sale_forecast.start_date = today
    sale_forecast.end_date = today
    sale_forecast.product = product
    sale_forecast.quantity = quantity
    sale_forecast.save()
    return sale_forecast
