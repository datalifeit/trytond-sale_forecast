datalife_sale_forecast
======================

The sale_forecast module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_forecast/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_forecast)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
